# MakeEC2VPN

## Short/Vague

Automatically creates an Amazon E.C.2. instance, uploads the 'InstallOpenVPNServer.bsh' script, runs it (to setup the 'OpenVPN' server) and (on the client) connects to the 'OpenVPN' server on this instance. If the script fails of the 'OpenVPN' command on the client stops, the instance is terminated.
Long/Detailed

1. Set a 'trap' which makes this script terminate the Amazon E.C.2. instance and remove any temporary files generated by this script if this script exits or a command in this script exits with an error.
1. Generate a 2048-bit R.S.A. key pair.
1. Launch an Amazon E.C.2. instance with a launch script that stops S.S.H., overwrites the S.S.H. daemon private and public keys with the ones generated locally, and restarts the daemon. On the local script, we don't wait for these changes to occur, we only wait for the A.W.S. command-line interface to say the command has been executed (instance started and startup script sent).
1. Determine the I.P. address of the Amazon E.C.2. instance from the instance I.D., making sure we have a non-null and non-private/local I.P. address.
1. Use ssh-keyscan to obtain the public R.S.A. key from the instance and compare it to the one we generated. This is done until they match, indicating that the private and public keys on the instance have been changed and we can continue.
1. Upload 'InstallOpenVPNServer.bsh' (current directory) to the instance.
1. Remotely execute 'InstallOpenVPNServer.bsh' (located at '/home/ubuntu/InstallOpenVPNServer.bsh').
1. Download '/home/ubuntu/client.ovpn' from the instance to './makeEC2VPN.ovpn'. This is the 'OpenVPN' client configuration file containing all information required to connect to the V.P.N.
1. Execute the 'reboot' command on the instance.
1. Run 'OpenVPN' using sudo, specifying the configuration file we downloaded as our config. 

## Usage Instructions

Modify the following variables at the top of the script:

* AWSKeyName - The E.C.2. key pair which will be assigned to this instance as if you were setting it up manually.
* AWSSecurityGroup - The E.C.2. security group I.D. which will be assigned to this instance; ports 22/tcp and 1194/udp must be accessible from the I.P. address you're connecting from.
* AWSSubnetID - The E.C.2. subnet I.D. which will be assigned to this instance.
* localSSHKeyName - The path to the private S.S.H. key which corresponds to the E.C.2. key pair given in 'AWSKeyName'. 

Ensure the following commands are available:

* aws - The Amazon Web Services command-line tools.
* ssh-keygen - Part of the 'OpenSSH' client.
* ssh-keyscan - Part of the 'OpenSSH' client.
* ssh - Part of the 'OpenSSH' client.
* scp - Part of the 'OpenSSH' client.
* openvpn - 'OpenVPN' 

Core utilities such as 'rm' are assumed. I used Bash version 4.4.12(1)-release but other relatively new versions should be fine as well.

Ensure you have run 'aws configure' and you have the suffient level of permissions through the A.W.S. command-line interface. I use a root-level access key on my machine but feel free to use an I.A.M. which is most tighly contrained.

Download the '(InstallOpenVPNServer)[https://gitlab.com/conorab/InstallOpenVPNServer]' project/script (note that this is under a different licence) 'decrypt' the signed tarball using G.P.G. Once extracted, the tarball will contain a named something like 'InstallOpenVPNServer_v1.bsh'. Rename this file to 'InstallOpenVPNServer.bsh' and put it in your current directory. This should be the same directory as the 'makeEC2VPN' script. Your current directory also needs to be writable as 'makeEC2VPN' will create temporary files (private and public keys for the instance) while it runs. Don't worry, it removes them once it's done.

Once all this is done, make sure you can run the 'openvpn' command (with arbitrary arguments) as root using sudo, and then run the 'makeEC2VPN' script. It will do the rest. 
