# Change Log

## Version 1

Signature from archive key:

	gpg: Signature made Sun 15 Dec 2019 22:45:00 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]

Signature from online key:

	gpg: Signature made Tue 30 Jan 2018 19:29:56 AEDT
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]
